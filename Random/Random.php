<?php
    
$url = "https://api.random.org/json-rpc/2/invoke";
$data = array(
    "method" => "generateDecimalFractions",
    "params" => array(
        "apiKey" => "ac3803cf-8bf2-4c02-92d3-de4ad8a10c0c",
        "n" => 1,
        "decimalPlaces" => 5,
        "replacement" => false
    ),
    "id" => 5185
);

$options = array(
    'http' => array(
        'header' => 'Content-type: application/json',
        'method' => 'POST',
        'content' => '{
            "jsonrpc": "2.0",
            "method": "generateDecimalFractions",
            "params": {
                "apiKey": "ac3803cf-8bf2-4c02-92d3-de4ad8a10c0c",
                "n": 1,
                "decimalPlaces": 5,
                "replacement": false
            },
            "id": 5185
        }'
    )
);

$context = stream_context_create($options);
$jsonResult = file_get_contents($url, false, $context);

if($jsonResult === false){
echo "error: " . $jsonResult;
};

$test = json_decode($jsonResult, true);

foreach($test as $one){
    if(is_countable($one)){
        foreach($one as $two){
            echo $two . "<br>";
        }
    }
}

for($iteratorOne = 0; $iteratorOne < count($length); $iteratorOne+= 1){
    if(is_countable($test[$iteratorOne])){
        for($iteratorTwo = 0; $iteratorTwo < count($test[$iteratorOne]); $iteratorTwo+= 1){
            echo $two . "<br>";
        }
    }
}

?>