<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
<link rel="stylesheet" href="/Public/Css/ProjectPage.css?v=1.1">

</head>
<body>
    
<?php
    include("../../View/Header.php");
?>

<!--Start of Webpage -->
<div class="WebpageContainer">

    
    <!-- first block "The Project" -->
    <div class="leftBlockWrapper">


      <div class="leftBlockExplenation">
          <!-- Title -->
          <p class="mediumHeaderText centeredText">
              The Project
          </p>
          <!-- the explenation of the Project -->
           <p class="normalText">
              I started this project because of my "Multiplayer test" project.<br>
              In that project I first used multithreading, and wanted to make a library for it, so I coult always easily implement it on new projects.<br>
              <br>
              This project is still not finished and I have a couple more things I want to implement in it.
            </p>
        </div>
    </div>

    <div style="clear: both; height: 7vh;"></div>

    <!-- second block "ThreadSystem" -->
    <div class="rightBlockWrapper">

        <div class="rightBlockImage">
            <img  style="height: 60vh; width: auto;" src="/Resources/Images/MultiThreadingLib/ThreadSystem.PNG">
        </div>

        <div class="rightBlockExplenation">
            <!-- Title -->
            <p class="mediumHeaderText centeredText">
                The ThreadSystem.
            </p>
            <!-- the explenation of the ThreadSystem -->
            <p class="normalText">
                The Threadsystem's purpose is to manage all of the jobs and threads.<br>
                With it you can Assign new jobs to be executed.<br>
                It also contains all the necessary functions needed for the jobs and threads, 
                and contains the loop that the threads run until the they are closed.
            </p>
        </div>
</div>
    
<div style="clear: both;"></div>

    <!-- third block "Dispatcher" -->
    <div class="leftBlockWrapper" style="clear: left;">
        <div class="leftBlockImage">
            <img  style="transform: translate(5vw, 0); height: 40vh; width: auto;" src="/Resources/Images/MultiThreadingLib/ThreadDispatcher.PNG">
        </div>

        <div class="leftBlockExplenation">
            <p class="mediumHeaderText centeredText">
                The ThreadDispatcher
            </p>
            <!-- the explenation of the Dispatcher -->
            <p class="normalText">
                When a job has been completed, something might need to happen on the main thread (for example if the Unity API needs to be used), this is where the ThreadDispatcher comes into play.<br>
                <br>
                You can can queue a method on the ThreadDispatcher, and the Dispatcher will then execute that method on the main thread.
            </p>
        </div>
    </div>
</div> <!-- End of Webpage-->

<div style="clear: both; height: 15vh;"></div>

</body>
</html>