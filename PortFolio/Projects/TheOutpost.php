<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
</head>

<body>
<?php
include("../../View/Header.php");
?>

<!--Start of Webpage -->
<div class="WebpageContainer">
    <h1 class="largeHeaderText centeredText">The Outpost</h1>
    
    <!-- first block image + text "The Outpost itself"-->
    <div style="clear: both; height: min-content;">
        <img style="border-radius: 25px;" class="outpostFirstImage" src="/Resources/Images/TheOutpost/InsideCapsule.png"/>

        <div class="outpostFirstTextBlock">
            <p class="smallHeaderText centeredText">
The Outpost</p>
            <p class="normalText">
This project was created by Roberto Peek in ~10 hours.<br>
The goal was to create gameplay by using assets from the Unity Asset Store and creating a level with them.<br>
<br>
<p class="smallNormalText">Note: this means that none of the assets, textures nor images used are mine.<br></p>
<br>
<p class="normalText">
<i>Target platform:</i> PC<br>
<i>Completed on:</i> 20-November-2019<br>
<i>Software used:</i> Unity
</p>
            </p>
        </div>
        <!-- download button the outpost -->
        <a href="/Resources/Projects/TheOutpost.zip" download="TheOutpost">
            <img src="/Resources/Images/DownloadButtonImage.png" class="downloadButtonTheOutpost"/>
        </a>
        <!-- Create a download button ? -->
    </div>

    <div style="clear: both; padding-top: 20px;padding-bottom: 20px; margin-left: 17.5vw">
<p class="largeHeaderText">Seperate game pieces</p>
    </div>

    <div style="clear: both;">
        <!-- Second block image + text "Lights"-->
        <div style="clear: both;">
            <video muted style="border-radius: 25px;" class="outpostLightsVideo" autoplay="autoplay" loop>
                <source src="/Resources/Videos/The_Outpost/TheOutpostLights.mp4" type="video/mp4">
            </video>
            <div class="outpostSecondTextBlock">
                <p class="smallHeaderText centeredText">
Flickering Lights
                </p>
                <p class="normalText">
Someting I really wanted to include in this game was a way to make lot's of light flicker, and I am pretty happy with how it turned out.<br>
<br>
These lights have 3 speed settings to which they can be set.<br>
The most forward lights in the video being set to "Fast", and the one in the back being to "Slow".<br>
                </p>
            </div>
        </div>
    </div>

    <!-- Third block image + text "Key system"-->
    <div style="clear: both; padding-top: 75px;">
        <div class="outpostThirdTextBlock">
            <p class="smallHeaderText centeredText">
Key System
            </p>
            <p class="normalText">
In The Outpost you have to progress to the next area by unlocking a door. I created a Key System for this.<br>
You can turn an object into a key and assign that key to a door, or to another key.<br>
The projector in the video unlocks the door towards the next area, but to use the projector, you need to first find the codes to acces the projector, and disable the acces lock.
            </p>
        </div>
            <video muted style="border-radius: 25px;" class="outpostKeySystemVideo" autoplay="autoplay" loop>
                <source src="/Resources/Videos/The_Outpost/TheOutpostKeySystem.mp4" type="video/mp4">
            </video>
    </div>
</div> <!-- End of Webpage-->

</body>