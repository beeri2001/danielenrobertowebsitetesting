<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
<link rel="stylesheet" href="/Public/Css/ProjectPage.css?v=1.1">

</head>
<body>
    
<?php
    include("../../View/Header.php");
?>

<!--Start of Webpage -->
<div href="#Default" class="WebpageContainer">

    
    <!-- first block "The Game" -->
    <div class="leftBlockWrapper">

      <img src="/Resources/Images/Hillplant/Preview.png" class="leftBlockImage" style="transform: translate(0%, 7vh);">

      <div class="leftBlockExplenation">
          <!-- Title -->
          <p class="mediumHeaderText centeredText">
              The Website.
          </p>
          <!-- the explenation of the Game -->
            <p class="normalText">
                I created the website together with a friend of mine.<br>
                I mostly focused on the back-end while my friend focused (mostly) on the front-end.<br>
                The website requires users to have an account (which is given to them by Hillplant) to use it.<br>
                <br>
                The website is currently live, and we are still gradually expanding on it.
            </p>
        </div>

    </div>

    <div style="clear: both; height: 7vh;"></div>

    <!-- second block "API" -->
    <div class="rightBlockWrapper" style="clear: left;">

    <img src="/Resources/Images/Hillplant/API_LoginCheck_Preview.png" class="rightBlockImage" style="transform: translate(0, 7vh); border-radius: 0px;">

        <div class="rightBlockExplenation">
            <p class="mediumHeaderText centeredText">
                The API.
            </p>
            <!-- the explenation of the Server -->
            <p class="largeNormalText">
                For this website there are a lot of things that should be saved in a database.<br>
            </p>
            <p class="normalText">
                Because of that I decided to create an API which handles all of that.<br>
                This API is my first attempt at making a "big" API.<br>
                <br>
                I learned a lot from making (and still adding onto) this API, namely:<br>
                - To, and how to structure the requests and responses that are sent and received from the API.<Br>
                - How to handle SQL injection.<br>
                - How to handle XSS (js) Injection (checking the sent in values for any html tags).<br>
                - Making certain actions only possible for someone's own account.
            </p>
        </div>
    </div>

    <div style="clear: both;"></div>

    <!-- third block "Accounts" -->
    <div class="leftBlockWrapper">

        <img src="/Resources/Images/Hillplant/LoginScreen.png" class="leftBlockImage" style="transform: translate(0, 7vh);">

        <div class="leftBlockExplenation">
            <!-- Title -->
            <p class="mediumHeaderText centeredText">
                Accounts.
            </p>
            <!-- the explenation of the Multiplayer -->
            <p class="largeNormalText">
                almost everything on the website requires having an acount.
            </p>
            <p class="normalText">
                <br>
                Which means we had to create an account system, I took this task upon me.<br>
                The accounts should obviously be completly secure, and we wanted to create a system where Hillplant can give accounts to their new employees, and not the employees creating their accounts themselves.<br>
                So when Hillplant wants to create a new account, they can "prepare" an account for the new employee.<br>
                The employee can then "activate" the account by adding their details to it.<br>
                After the employee has activated the account, Hillplant can then accept it, after which, the account can be used.<br>
                <br>
                I added functions to the API which can handle all actions required for the accounts.
            </p>
        </div>
    </div>

    <div style="clear: both; height: 5%;"></div>
    
    <!-- fourth Block "Planning" -->
    <div class="rightBlockWrapper">
        
    <img class="rightBlockImage" style="transform: translate(0%, 8vh);" src="/Resources/Images/Hillplant/Planning.png"/> 

        <div class="rightBlockExplenation">
            <!-- Title -->
            <p class="mediumHeaderText centeredText">
                Planning.
            </p>
            <p class="largeNormalText">
                The planning is a function which every employee can use.<br>
            </p>
            <!-- the explenation of the Planning -->
            <p class="normalText">
                Every empoyee can input their work planning into the table, their workdays and times are then saved in the database.<br>
                <br>
                As dates shouldn't be changed last minute, Hillplant can select a date, and from that date on all employees can input their times and dates.<Br>
                <br>
                Hillplant can download the planning into a CSV file which they can then read in excel to get the planning up until a desired date.<br>
                <br>
                For the planning, I created functions in the API which can be used to fetch / set their planning data from / into the database.<br>
                I also created the script that outputs the CSV file containg the planning.
            </p>
        </div>   

    </div>

</div> <!-- End of Webpage-->

</body>
</html>