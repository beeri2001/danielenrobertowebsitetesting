<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
<link rel="stylesheet" href="/Public/Css/ProjectPage.css?v=1.1">

</head>
<body>
    
<?php
    include("../../View/Header.php");
?>

<!--Start of Webpage -->
<div href="#Default" class="WebpageContainer">

    
    <!-- first block "The Game" -->
    <div class="leftBlockWrapper">

      <img src="/Resources/Images/MultiplayerTest/TopDownGameView.PNG" class="leftBlockImage" style="transform: translate(0%, 10vh);">

      <div class="leftBlockExplenation">
          <!-- Title -->
          <p class="mediumHeaderText centeredText">
              The Game.
          </p>
          <!-- the explenation of the Game -->
           <p class="normalText">
              The game itself is pretty simple, the players can walk around to collect the green orbs to gain points.<br>
              There is no winning or losing in this game, as I wasn't focussed on it.<br><br>
            </p>
            <p class="smallNormalText">
              It is not finished yet, there are still problems with the game, for example:<br> <br>-collectible not despawning correctly when a player leaves <br>-spawning too many collectibles  <br>-the collecitbles disappearing and reappearing.
            </p>
        </div>

        <a style="float: right; margin-right: 15vw; margin-top: 10vh; margin-bottom: 0px;" href="/Resources/Projects/Multiplayer_Test.zip" download="Multiplayer_Test">
            <img style="height: 10vh;border-radius: 125px;" src="/Resources/Images/DownloadButtonImage.png">
        </a>

    </div>

    <div style="clear: both; height: 7vh;"></div>

    <!-- second block "Multiplayer" -->
    <div class="rightBlockWrapper">

        <div class="rightBlockImage">
            <img  style="height: 33vh; width: auto;" src="/Resources/Images/MultiplayerTest/Requests/PlayerDataReqest.PNG"/>
            <div style="height: 5vh;"></div>
            <img  style="height: 33vh; width: auto;" src="/Resources/Images/MultiplayerTest/Requests/API_PlayerDataRequestProcessor.PNG"/>

        </div>
        <div class="rightBlockExplenation">
            <!-- Title -->
            <p class="mediumHeaderText centeredText">
                Multiplayer.
            </p>
            <!-- the explenation of the Multiplayer -->
            <p class="largeNormalText">
                Multiplayer was the main focus of this game.
            </p>
            <p class="normalText">
                <br>
                I had figured out how to post and request data from a database, and wanted to try creating a multiplay game with it.<br>
                From the beginning I knew that you should actually use sockets for multiplayer, but I wanted to try it anyway (although I do want to learn about sockets aswell).<br><br>
                I learnt a great deal about requesting and posting via this project, aswell as the time it takes to make a request,<br>
                which was the reason I went on to multithreading to speed the game up.<br>
                And if the request is to be processed, an API of some sorts is also needed, so I created a small API to process the request the game makes.
            </p>
        </div>
</div>
    
<div style="clear: left;"></div>

    <!-- third block "server" -->
    <div class="rightBlockWrapper" style="clear: left;">

        <div class="rightBlockExplenation">
            <p class="mediumHeaderText centeredText">
                Server (substitute).
            </p>
            <!-- the explenation of the Server -->
            <p class="normalText">
                I did not create an actual server that would keep track of the game, so the game does that itself (as a substitute server).<br>
It looks at what data (players and collectibles) it currently knows about, and compares that to what it receives from the database.<br>
And based on that, it checks every collectible and player for where it should move, if it should still exists, what score it is, etc.<br><br>
It also posts the changes that were made on it's own players game to the database, handles players quitting, and collectibles being picked up (although some of these are still work in progress).
            </p>
        </div>
    </div>

    <div style="clear: both; height: 5%;"></div>
    
    <!-- fourth Block "Multithreading" -->
    <div class="leftBlockWrapper">
        
    <img class="leftBlockImage" style="transform: translate(0%, 8vh); height: 20vh; width: auto; border-radius: 0px;" src="/Resources/Images/MultiplayerTest/Threading/ThreadCreationCode.PNG"/> 

        <div class="leftBlockExplenation">
            <!-- Title -->
            <p class="mediumHeaderText centeredText">
                Multithreading.
            </p>
            <!-- the explenation of the Multithreading -->
            <p class="normalText">
                When I got most of the game working, it was lagging significantly.<br>
To counteract this, I decided I wanted to learn how to use multithreading, reading up on it, and trying to implement it into the game.<br><br>
After about 2 small but failed attempts, I felt I got the hang of it, and refactored most of the server and connections to work with the multithreading.<br>
It's not perfect, but it was good enough for this project, and I will be implementing it in more projects to come, learning along the way.<br>
            </p>
        </div>   

    </div>

</div> <!-- End of Webpage-->

</body>
</html>