<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
    <link rel="stylesheet" href="/Public/Css/ProjectPage.css?v=1.1">
</head>
<body>

<?php
    include("../../View/Header.php");
?>
    
<!--Start of Webpage -->
<div class="WebpageContainer">

    <!--
- Why I built the system
explenation of why I built the project for school

- The System
Display the game on the right, with an explenation of the system to the left

- The UI and the Camera movement
explenation of what I did for the UI and the camera movement
    -->
    
    <!-- System explenation block -->
    <div class="rightBlockWrapper">
        <!-- Preview video -->
            <video loop muted autoplay="autoplay" class="rightBlockImage" src="/Resources/Videos/L-System/L-System System Preview.mp4"></video>

            <div class="rightBlockExplenation">
                <!-- Title -->
                <p class="mediumHeaderText centeredText">
                    The System.
                </p>
                <!-- the explenation of the system -->
                <p class="normalText">
I created this L-system for a school project, but decided to expand upon the original task.
Ofcourse I started with the system itself, which eventually got to where it is now.<br><br>
I was pretty happy with the result, and so decided to try building it to a web based game (using WebGL), 
and made it so users could input their own rules into it using the UI.
                </p>
            </div>
    </div>

    <div style="height: 20px; clear: both;"></div>

    <div class="gameBlock">
        <div class="gameBlockText">
            <p class="largeHeaderText centeredText">
                The game
            </p>
        </div>

        <!-- The game itself -->
        <div class="gameView"> <!-- de height moet iets groter dan de width voor de footer -->
            <iframe style="position: relative;" src="/Resources/Projects/L-System/index.php" height="110%" width="100%" frameborder="0"> </iframe style="position: relative;" >
        </div>  
    </div>

    <!-- whitespace -->
    <div style="clear: both; height: 5%;"></div>

    <div class="leftBlockWrapper">
        <div class="leftBlockExplenation">
            <!-- introduction -->
            <p class="largeHeaderText centeredText">
            UI and the Camera
            </p>
            <p class="normalText">
                For this project I decided to create a UI and camera that would work well with the system.<br>
            </p>
            <!-- camera bit -->
            <p class="smallHeaderText">
                Camera:
            </p>
            <p class="normalText">
                The system can be made bigger or smaller so I needed to create a camera that would work for both.
            </p>
            <!-- UI bit -->
        </p>
        <p class="smallHeaderText">
                UI:
            </p>
            <p class="normalText">
                The user needs to be able to input rule sets into the system, so I created a number of UI elements whose values could be entered easily.
            </p>
        </div>

        <video class="leftBlockImage" muted loop autoplay="autoplay" src="/Resources/Videos/L-System/L-System Camera Preview.mp4"></video>
    </div>


</div> <!-- End of Webpage -->

</body>

</html>