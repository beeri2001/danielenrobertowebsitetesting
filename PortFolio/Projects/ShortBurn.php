<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
<link rel="stylesheet" href="/Public/Css/ProjectPage.css?v=1.1">

</head>
<body>

<?php
    include("../../View/Header.php");
?>

<!--Start of Webpage -->
<div class="WebpageContainer">

    
    <!-- first block "The Project" -->
    <div class="leftBlockWrapper">


      <div class="leftBlockExplenation">
          <!-- Title -->
          <p class="mediumHeaderText centeredText">
              The Project
          </p>
          <!-- the explenation of the Project -->
           <p class="normalText">
              For this project we started with an already existing "game" Micro FPS made by Unity.<br>
              We then expanded upon that project by adding our own level, mechanics and puzzles.<br>
            </p>
        </div>
    </div>

    <div style="clear: both; height: 7vh;"></div>

    <!-- second block "Interaction system" -->
    <div class="rightBlockWrapper">

        <div class="rightBlockImage">
            <video muted style="border-radius: 25px;" class="projectImageRight" autoplay="autoplay" loop>
				<source src="/Resources/Videos/ShortBurn/Cell 2 interaction.mp4" type="video/mp4">
			</video>
        </div>

        <div class="rightBlockExplenation">
            <!-- Title -->
            <p class="mediumHeaderText centeredText">
                Interaction System.
            </p>
            <!-- the explenation of the ThreadSystem -->
            <p class="normalText">
                I created an interaction system for the player with which other objects could easily be made to do things.<br>
                The user can interact with different objects in front of them by pressing a button.
            </p>
        </div>
</div>
    
<div style="clear: both;"></div>

    <!-- third block "Savable States" -->
    <div class="leftBlockWrapper" style="clear: left; transform: translate(5%, 0);">
        <div class="leftBlockImage">
            <video muted style="border-radius: 25px;" class="projectImageRight" autoplay="autoplay" loop>
				<source src="/Resources/Videos/ShortBurn/SaveableStates.mp4" type="video/mp4">
			</video>
        </div>

        <div class="leftBlockExplenation">
            <p class="mediumHeaderText centeredText">
                Savable States
            </p>
            <!-- the explenation of the Dispatcher -->
            <p class="normalText">
                We needed a system for respawning, so I decided to create one.<br>
                Player states can be saved as a checkedpoint by other objects, and states of other objects can also be saved.<br>
                When level is loaded (or reloaded when the player dies) the system looks for saved states for objects or the player, and applies them.
            </p>
        </div>
    </div>

    <div style="clear: both;"></div>

    <!-- fourth block "Enemy Spawning" -->
    <div class="rightBlockWrapper" style="clear: left;">
        <div class="rightBlockImage">
            <video muted style="border-radius: 25px;" class="projectImageRight" autoplay="autoplay" loop>
				<source src="/Resources/Videos/ShortBurn/Enemy Spawning.mp4" type="video/mp4">
			</video>
        </div>

        <div class="rightBlockExplenation">
            <p class="mediumHeaderText centeredText">
                Enemy Spawner
            </p>
            <!-- the explenation of the Dispatcher -->
            <p class="normalText">
                The enemies in the seconds part of the level are (mostly) spawned in when you play.<br>
                I created a script to spawn them in, and gave it locations to spawn them on.<br>
                The script tries to spawn the enemies on a location that the player cannot see.<br>
            </p>
        </div>
    </div>
</div> <!-- End of Webpage-->

<div style="clear: both; height: 15vh;"></div>

</body>
</html>