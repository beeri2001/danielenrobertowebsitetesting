<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
</head>
<body>
<?php
include("../View/Header.php");
?>

<!--Start of Webpage -->
<div class="WebpageContainer">


<!-- Dutch donwload Roberto Peek CV Button -->
<a href="/Resources/Personal/CV Roberto Peek (Nederlands).pdf" target="_blank"> <!-- target="_blank" - makes the page open in a new tab -->
    <img class="downloadRobertoCVButtonNL" src="/Resources/Images/DownloadRobertoCVButtonNL.png"/>
</a>

<!-- English donwload Roberto Peek CV Button -->

<a  href="/Resources/Personal/CV Roberto Peek (English).pdf" target="_blank">
<img class="downloadRobertoCVButtonEN" src="/Resources/Images/DownloadRobertoCVButtonEN.png"/>
</a>

<!-- Main text block -->
<div style="width: 65vw; right: 50vw; transform: translate(20%, 0%);">
    <p class="smallHeaderText centeredText">Roberto Peek</p>
    
    <p class="normalText">
    Hello! My name is Roberto Peek, and I live in the Netherlands.<br>
    I'm currently studying to become a game developer at Grafisch Lyceum Utrecht.<br>
    I've been playing video games for as long as I can remember, so when I graduated from high school I just knew I wanted to follow this course.<br>
    <br>
    My interests are mostly with Unity C#, PHP,sql and connecting all these together.<br>
    I'm still learning more about networking and PHP in general.<br>
    </p>

</div>

<!-- Roberto's Picture -->
<div style="margin-left: 50vw;">
    <img class="robertoPictureOnMainScreen" src="/Resources/Images/Roberto.png"/>
</div>

</div> <!-- End of Webpage-->

</body>
</html>