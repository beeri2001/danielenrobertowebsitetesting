<!DOCTYPE html>

<head>
	<link rel="stylesheet" href="/Public/Css/Main.css?v=1.1">
</head>

<body>
<?php
include("../View/Header.php");
?>

<!--Start of Webpage -->
<div href="#Default" class="WebpageContainer">

	<!-- The main screen parallax -->
	<!-- <div id="Default" class="mainScreenParralax"></div> -->

	<!-- Dutch donwload Roberto Peek CV Button -->
	<a href="/Resources/Personal/CV Roberto Peek (Nederlands).pdf" target="_blank"> <!-- target="_blank" - makes the page open in a new tab -->
		<img class="downloadRobertoCVButtonNL" src="/Resources/Images/DownloadRobertoCVButtonNL.png"/>
	</a>

	<!-- English donwload Roberto Peek CV Button -->

	<a  href="/Resources/Personal/CV Roberto Peek (English).pdf" target="_blank">
		<img class="downloadRobertoCVButtonEN" src="/Resources/Images/DownloadRobertoCVButtonEN.png"/>
	</a>

	
		<!-- Roberto's Picture -->
	<!-- <div class="robertoPictureOnMainScreenWrapper">
		<img class="robertoPictureOnMainScreen" src="/Resources/Images/Roberto.png"/>
	</div> -->

	<!-- Welcome Header -->
	<!-- <p class="centeredText largeHeaderText" style=>Roberto Peek<br></p>
	<p class="centeredText smallHeaderText" style="transform: translate(0px, -40px); margin-right: 20vw; margin-left: 20vw;""><b>Currently studying to become a game developer</b></p>

	<p class="smallHeaderText centeredText">
		Contact me:<br>
	</p>
	<p class="largeNormalText centeredText">
		e-mail: peekroberto@gmail.com
	</p>

	<div style="height: 10vh;"></div> -->

	<!-- Projects Header-->
	<h2 class="mediumHeaderText" style="margin-top: 50px"><center>My projects:</center></h2>


	<!-- Project Description Multiplayer Test game -->
	<div style="clear: both; margin-top: 75px;">
				<div>
						<a href="/PortFolio/Projects/MultiplayerTest.php">
							<img class="projectImageRight" style="border-radius: 25px;" src="/Resources/Images/MultiplayerTest/PreviewImage.PNG"/>
						</a>
					</div>
			<div class="projectDescriptionRight normalText">
				<p class="smallHeaderText centeredText">Multiplayer Test (and multithreading)</p>
				This project is my first attempt at a  multiplayer game, although I know I didn't do it in the correct way.<br>
				I started this project when I learned about http requests and databases, and decided to try and use that to create something multiplayer.
				<br>
				Not using sockets resulted in pretty slow requests, which I wanted to speed up, so I ended up learning about multithreading, which ended up as a big part of this project.<br>
				<br>
				<i>Target platform:</i> PC, Windows<br>
				<i>Completed on:</i> 3-Febuary-2020<br>
				<i>Software used:</i> Unity
				<br>
			 <a href="/PortFolio/Projects/MultiplayerTest.php"><p class="largeNormalText hyperlinkText" >Go to page</p></a>
		</div>
	</div>

	<div style="clear: both; height: 150px;"></div> <!-- a bit of spacing towards the next project preview	 -->

	<!-- Project Description Hillplant Website -->
	<div style="clear: both">
				<div>
					<a href="/PortFolio/Projects/HillplantWebsite.php">
						<img class="projectImageLeft" style="border-radius: 25px;" src="/Resources/Images/Hillplant/Preview.png"/>
					</a>
				</div>
			<div class="projectDescriptionLeft normalText">
				<p class="smallHeaderText centeredText">Hillplant Website</p>
				Hillplant has asked me and a friend to create a website for them.<br>
				The website should function to simplify certain aspects within the company.<br>
				Together with Hillplant we created and launched the website, and are still planning on expanding upon it.
				<br>
				<br>
				<i>Target platform:</i> Browser<br>
				<i>Completed on:</i> Live, but being expanded upon<br>
				<br>
			 <a href="/PortFolio/Projects/HillplantWebsite.php"><p class="largeNormalText hyperlinkText" >Go to page</p></a>
		</div>
	</div>

	<div style="clear: both; height: 150px;"></div> <!-- a bit of spacing towards the next project preview	 -->

	<!-- Project Description Short-Burn -->
	<div style="clear: both">
		<div>
				<a href="/PortFolio/Projects/ShortBurn.php">
					<img class="projectImageRight" style="border-radius: 25px;" src="/Resources/Images/ShortBurn/Preview.png"/>
				</a>
		</div>
			<div class="projectDescriptionRight normalText">
			<p class="smallHeaderText centeredText">Short-Burn</p>
				Short-Burn is a group project that I worked on during the end of my second year studying game development.<br>
				We used Unity's FPS Micro game as a template, which we then expanded upon.<br>
				<br>
				<i>Target platform:</i> PC, Windows<br>
				<i>Completed on:</i> 26-June-2020<br>
				<i>Software used:</i> Unity
				<br>
	 		<a href="/PortFolio/Projects/ShortBurn.php"><p class="largeNormalText hyperlinkText" >Go to page</p></a>
		</div>
	</div>

<div style="clear: both; height: 150px;"></div> <!-- a bit of spacing towards the next project preview	 -->

	<!-- Multithreading WIP project description -->
	<div style="clear: both">
		<div>
			<a href="/PortFolio/Projects/MultithreadingLib.php">
				<img class="projectImageLeft" style="border-radius: 25px;" src="/Resources/Images/MultiThreadingLib/Preview.PNG"/>
			</a>
		</div>
	<div class="projectDescriptionLeft normalText">
		<p class="smallHeaderText centeredText">Multithreading Library (Work in progress)</p>
		After making my "Multiplayer test" project I wanted to make a "Library"  
		to handle all the multithreading for me, that way I could easily implement it anywhere.<br>
		It is not completed yet, and I am working on it casually.<br>
		<br>
		<i>Target platform:</i> Unity 2019+<br>
		<i>Software used:</i> Unity
		<br>
 		<a href="/PortFolio/Projects/MultithreadingLib.php"><p class="largeNormalText hyperlinkText" >Go to page</p></a>
	</div>
</div>

<div style="clear: both; height: 150px;"></div> <!-- a bit of spacing towards the next project preview	 -->

	<!-- Project Description L-System -->
	<div style="clear: both">
			<div>
					<a href="/PortFolio/Projects/L-System.php">
						<video muted style="border-radius: 25px;" class="projectImageRight" autoplay="autoplay" loop>
							<source src="/Resources/Videos/L-System/L-system Preview.mp4" type="video/mp4">
						</video>
					</a>
				</div>
		<div class="projectDescriptionRight normalText">
			<p class="smallHeaderText centeredText">L-System</p>

			<i>About this project and what I did:</i><br>
			For a school project we had to create an L-System,
			which I worked on quite a bit to get it to where it is now.<br>
			It is a very different kind of project from the things I've done before making this.<br>
			The main focus was ofcourse on the actual system,<br>
			but I also spent quite a bit of time to create the UI for the inputs,
			aswell as creating the movement for the camera.
			<br><br>
			<i>Target platform:</i> Websites (WebGL)<br>
			<i>Completed on:</i> 22-Januari-2020<br>
			<i>Software used:</i> Unity
			<br>
		 <a href="/PortFolio/Projects/L-System.php"><p class="largeNormalText hyperlinkText" >Go to page</p></a>
		</div>
	</div>

	<div style="clear: both; height: 150px;"></div> <!-- a bit of spacing towards the next project preview	 -->

	<!-- Project Description The Outpost -->
	<div style="clear: both">
			<div>
					<a href="/PortFolio/Projects/TheOutpost.php">
						<video muted style="border-radius: 25px;" class="projectImageLeft" autoplay="autoplay" loop>
							<source src="/Resources/Videos/The_Outpost/TheOutpostSlideshow.mp4" type="video/mp4">
						</video>
					</a>
				</div>
		<div class="projectDescriptionLeft normalText">
			<p class="smallHeaderText centeredText">The Outpost</p>

			<i>About this project and what I did:</i><br>
			For this project I took a multitude of free assets from the Unity Asset Store used them to create a game.
			The focus of this project was mostly to create a map and adding gameplay to it, together with getting better at game building in general.<br>
			I created the gameplay, and put the level together using the assets.<br>
			<br>
		 	<p class="smallNormalText">Note: this means that none of the assets, textures nor images used are mine.<br></p>
			<br>
			<i>Target platform:</i> PC, Windows<br>
			<i>Completed on:</i> 20-November-2019<br>
			<i>Software used:</i> Unity
			<br>
		 <a href="/PortFolio/Projects/TheOutpost.php"><p class="largeNormalText hyperlinkText" >Go to page</p></a>
		</div>
	</div>
</div> <!-- End of Webpage-->

</body>
