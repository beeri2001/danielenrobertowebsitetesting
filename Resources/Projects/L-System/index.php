<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Unity WebGL Player | L-System</title>
    <link rel="shortcut icon" href="TemplateData/favicon.ico">
    <link rel="stylesheet" href="TemplateData/style.css">
    <script src="TemplateData/UnityProgress.js"></script>
    <script src="Build/UnityLoader.js"></script>
    <script>
      var unityInstance = UnityLoader.instantiate("unityContainer", "Build/22-1-2020_09.13.json", {onProgress: UnityProgress});
    </script>
  </head>
  <body>
    <div style="user-select: none; -moz-user-select: none; -webkit-user-drag: none; -webkit-user-select: none; -ms-user-select: none;" 
    class="webgl-content">
      <div id="unityContainer" style="width: 100%; height: 100%"></div>
      <div class="footer">
        <div class="fullscreen" onclick="unityInstance.SetFullscreen(1)"></div>
      </div>
    </div>
  </body>
</html>