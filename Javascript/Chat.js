console.log(location.protocol + "//" + location.hostname + "/Node");

// var socket = io.connect(location.protocol + "//" + location.hostname + "/Node");


// make connection.
var socket = io.connect("localhost/Packages/node_modules");

var debugSocket = socket.socket;

//query DOM.
var message = document.getElementById("message");
var handle = document.getElementById("handle");
var button = document.getElementById("send");
var output = document.getElementById("output");
var feedback = document.getElementById("feedback");

//emit events.
button.addEventListener("click",function(){
    socket.emit("chat", {
        message: message.value,
        handle: handle.value,
    });
});

message.addEventListener("keypress", function(){
    socket.emit("typing", handle.value);
});

//listen for events.
socket.on("chat", function(data){
    feedback.innerHTML = "";
    output.innerHTML += "<p><strong>" + data.handle + ":</strong> " + data.message + "</p>";
});

socket.on("typing", function(data){
    feedback.innerHTML = "<p><em>" + data + " is typing...</em></p>";
});

// (fired from client) On error when connecting.
debugSocket.on("connect_failed", function() {
    console.log("Connection failed (d)");
});

debugSocket.on("connect_error", function() {
    console.log("Connection failed (d)");
});

debugSocket.on("error", function(){
    console.log("error (d)");
})

// (fired from client) On error when connecting.
socket.on("connect_failed", function() {
    console.log("Connection failed (s)");
});

socket.on("connect_error", function() {
    console.log("Connection failed (s)");
});

socket.on("error", function(){
    console.log("error (s)");
})