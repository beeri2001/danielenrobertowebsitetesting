var express = require("../Packages/node_modules/express");
var socket = require("../Packages/node_modules/socket.io");

//app setup
var app = express();
var server = app.listen(4000, function() {
    console.log("listening to request");
});

//static files.
app.use(express.static("../View"));

//socket setup.
var io = socket(server).listen(server);
console.log(server);

// On successful connection made.
io.on("connection", function(socket){
    console.log("made socket connection", socket.id);

    socket.on("chat", function(data){
        io.sockets.emit("chat", data);
    });

    socket.on("typing", function(data){
        socket.broadcast.emit("typing", data);
    })
});

