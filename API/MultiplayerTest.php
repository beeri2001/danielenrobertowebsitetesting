<?php
/* Post PlayerData into the database */
if($_POST['Command'] == "PostPlayerData"){
$ranCode = true;

// get the given varialbe
$Score = $_POST['Score'];
$ID = $_POST['ID'];
$X = $_POST['X'];
$Y = $_POST['Y'];
$Z = $_POST['Z'];

// make floats culture invariant for the DB
$X = str_replace(",", ".", $X);
$Y = str_replace(",", ".", $Y);
$Z = str_replace(",", ".", $Z);
// get the database connections script
include("../Model/Model.php");
// connect to the database
$conn = ConnectToDatabase("MultiplayerTest");
// post the values to the database
ExecuteCommandOnDatabase($conn, "INSERT INTO `PlayerData`(`x`, `y`, `z`, `id`, `score`) 
VALUES ($X, $Y, $Z, $ID, $Score) ON DUPLICATE KEY UPDATE `score`=$Score,`x`=$X,`y`=$Y,`z`=$Z"); 
}

/* get all positions in the database */
if($_POST['Command'] == "GetAllPlayerData" || $_GET['command'] == "GetAllPlayerData"){

    $ranCode = true;
    
    include("../Model/Model.php");
    
    $conn = ConnectToDatabase("MultiplayerTest");
    $data = GetAllDataFromTable($conn, "PlayerData");
    
    $jsonString = json_encode($data);
    
    echo $jsonString;
    
}

if($_POST['Command'] == "RemovePlayer" || $_GET['command'] == "RemovePlayer"){
    $ranCode = true;

    $ID = $_POST['ID'];

    include("../Model/Model.php");

    $conn = ConnectToDatabase("MultiplayerTest");
    $echoed = ExecuteCommandOnDatabase($conn, "DELETE FROM `PlayerData` WHERE id=$ID");

    echo $echoed;
}

if($_POST['Command'] == "PostCollectibleData"){
    $ranCode = true;

    $ID = $_POST['ID'];
    $X = $_POST['X'];
    $Y = $_POST['Y'];
    $Z = $_POST['Z'];
    $SpawnedBy = $_POST['SpawnedBy'];

    $X = str_replace(",", ".", $X);
    $Y = str_replace(",", ".", $Y);
    $Z = str_replace(",", ".", $Z);

    include("../Model/Model.php");

    $conn = ConnectToDatabase("MultiplayerTest");
    ExecuteCommandOnDatabase($conn, "INSERT INTO `Collectible`(`x`, `y`, `z`, `id`, `spawnedBy`) VALUES ($X, $Y, $Z, $ID, $SpawnedBy)");
}

 // this works
if($_POST['Command'] == "GetAllCollectibles"){
    $ranCode = true;
    
    include("../Model/Model.php");
    
    $conn = ConnectToDatabase("MultiplayerTest");
    $data = GetAllDataFromTable($conn, "Collectible");
        
    $jsonString = json_encode($data);
    
    echo $jsonString;
}

if($_POST['Command'] == "RemoveCollectible" || $_GET['command'] == "RemoveCollectible"){
    $ranCode = true;

    $ID = $_POST['ID'];

    include("../Model/Model.php");

    $conn = ConnectToDatabase("MultiplayerTest");
    $echoed = ExecuteCommandOnDatabase($conn, "DELETE FROM `Collectible` WHERE id=$ID");

    echo $echoed;
}

if(!$ranCode){
    echo "No Command was recognised, nothing returned";
}

$ranCode = false;