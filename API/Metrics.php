<?php
/* Post a new position into the database */
if($_POST['Command'] == "PostPosition" || $_GET['command'] == "PostPosition"){
$ranCode = true;
$X = $_POST['x'];
$Y = $_POST['y'];
$Z = $_POST['z'];
$R = $_POST['r'];
$G = $_POST['g'];
$B = $_POST['b'];

if($X == null && $Y == null && $Z == null){
$X = $_GET['x'];
$Y = $_GET['y'];
$Z = $_GET['z'];
}

$X = str_replace(",", ".", $X);
$Y = str_replace(",", ".", $Y);
$Z = str_replace(",", ".", $Z);

include("../Model/Model.php");

$conn = ConnectToDatabase("Metrics");
ExecuteCommandOnDatabase($conn, "INSERT INTO `PlayerPositions`(`x`, `y`, `z`, `r`, `g`, `b`) VALUES ($X, $Y, $Z, $R, $G, $B)");
}

/* get all positions in the database */
if($_POST['Command'] == "GetAllPositions" || $_GET['command'] == "getallpositions"){

$ranCode = true;

include("../Model/Model.php");

$conn = ConnectToDatabase("Metrics");
$data = GetAllDataFromTable($conn, "PlayerPositions");

$jsonString = json_encode($data);

echo $jsonString;

}
if(!$ranCode){
    echo "No Command was recognised, nothing returned";
}

$ranCode = false;
?>